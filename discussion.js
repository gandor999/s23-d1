// CRUD Operations

/*
		Create - Insert
		Read - Find


*/




// Inserting Documents (Create)

// Syntax: db.collectionName.insertOne({object});
// Javascript: object.object.method({object_properties});

db.users.insertOne({
           
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
                "phone": "0912876542",
                "email": "janedoe@mail.com"
            },
        "courses": [
                "CSS",
                "Javascript",
                "Pyhton"
            ],
        "department": "none"
    });




// Insert Many
// Syntax: db.collectionName.insertMany([{objectA}, {objectB}]);

db.users.insertMany([{
           
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
                "phone": "09236574873",
                "email": "stepehenhawking@mail.com"
            },
        "courses": [
                "React",
                "PHP",
                "Pyhton"
            ],
        "department": "none"
    
    
},
{
    "firstName": "Neil",
    "lastName": "Armstrong",
    "age": 82,
    "contact": {
            "phone": "092465738261",
            "email": "neilarmstrong@mail.com"
        },
    "courses": [
            "React",
            "Laravel",
            "SASS"
        ],
    "department": "none"
}

]);








// Read Operation

/*
		Find Syntax:
			db.collectionName.find();
			db.collectionName.find({field: value});
*/
 
db.users.find();
db.users.find({"lastName": "Doe"});
db.users.find({"lastName": "Doe", "age": 25}).pretty();








// Updating Document (Update) Operation

// db.collectionName.updateOne({critera}, {$set: {field: value}});


// Insert Sample test documunet

db.users.insertOne({
    
        "firstName" : "Test",
        "lastName" : "Test",
        "age" : 0,
        "contact" : {
            "phone" : "00000000000",
            "email" : "test@mail.com"
        },
        "courses" : [],
        "department" : "none"
    
    });



// Update one document


db.users.updateOne(
        {"firstName": "Test"},
    {
        $set: {
            "firstName" : "Bill",
            "lastName" : "Gates",
            "age" : 65,
            "contact" : {
                "phone" : "09178462839",
                "email" : "bill@mail.com"
            },
            "courses" : ["PHP", "Laravel", "HTML"],
            "department" : "none"
        }
    }
);


// Update multiple documents

db.users.updateMany(
        {"department": "none"},
        
        {
        $set: {
                "department" : "HR"
            }
        }
);









// Deleting Documents (Delete) Operation
// Syntax: db.collectionName.deletOne({criteria}); - deleting a single document

// Create a single coument with one field
db.users.insertOne({
    
        "firstName": "test"
    
});

// Delete one document
db.users.deleteOne({
    
        "firstName": "test"
    
});

// Update many first to operations
db.users.updateMany(
        {"lastName": "Doe"},
        
        {
        $set: {
                "department" : "Operations"
            }
        }
);


// Delete all documents or profiles that has the following department

db.users.deleteMany({
    
        "department": "Operations"
    
});







// Advance Query

// Query and embedded document
db.users.find({
	"contact": {
		"phone" : "09178462839",
		"email" : "bill@mail.com"
	}
});


// Querying an Array without a specific order of elements
db.users.find({"courses": { $all: ["React", "Python"]}});



A query is a request for data or information from a database table or combination of tables.